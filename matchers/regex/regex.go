/*
Package regex is a simple regex Matcher
*/
package regex

import (
	"regexp"

	"gitlab.oit.duke.edu/drews/redacter/matchers"
	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// Regex is the struct for this matcher
type Regex struct {
	match string
}

// Regex returns the regex for a match
func (r Regex) Regex() *regexp.Regexp {
	return regexp.MustCompile(r.match)
}

// Obfuscator returns the default obfuscator for this matcher
func (r Regex) Obfuscator() obfuscators.Creator {
	// No default obfuscator here
	return nil
}

var _ matchers.Matcher = Regex{}

func init() {
	matchers.Add("regex", func(m string) matchers.Matcher {
		return &Regex{match: m}
	})
}
