/*
Package ipv4 is a matcher for ipv4 addresses
*/
package ipv4

import (
	"regexp"

	"gitlab.oit.duke.edu/drews/redacter/matchers"
	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// IPv4 is the struct for this matcher
type IPv4 struct{}

// Regex returns the regex for a match
func (i IPv4) Regex() *regexp.Regexp {
	// return regexp.MustCompile(`(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])+`)
	return regexp.MustCompile(`((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}`)
}

// Obfuscator returns the default obfuscator for this matcher
func (i IPv4) Obfuscator() obfuscators.Creator {
	return obfuscators.Obfuscators["ipv4"]
}

var _ matchers.Matcher = IPv4{}

func init() {
	matchers.Add("ipv4", func(m string) matchers.Matcher {
		return &IPv4{}
	})
}
