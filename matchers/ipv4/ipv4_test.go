package ipv4

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIPv4Matcher(t *testing.T) {
	tests := map[string]struct {
		given  string
		expect bool
	}{
		"simple":          {given: "8.8.8.8", expect: true},
		"no-match":        {given: "foo", expect: false},
		"prefixed":        {given: "pre 8.8.8.8", expect: true},
		"suffixed":        {given: "8.8.8.8 suf", expect: true},
		"multi-num-trail": {given: "1.2.3.243", expect: true},
		"mac":             {given: "00:11:22:00:", expect: false},
	}

	for desc, tt := range tests {
		// IPv4{}.Regex()
		got, _ := regexp.Match(IPv4{}.Regex().String(), []byte(tt.given))
		require.Equal(t, tt.expect, got, desc)
	}
}
