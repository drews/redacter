/*
Package all just imports all the matchers
*/
package all

import (
	_ "gitlab.oit.duke.edu/drews/redacter/matchers/ipv4" // import plugins
	_ "gitlab.oit.duke.edu/drews/redacter/matchers/macaddress"
	_ "gitlab.oit.duke.edu/drews/redacter/matchers/regex"
)
