/*
Package macaddress describes how to match a macaddress
*/
package macaddress

import (
	"regexp"

	"gitlab.oit.duke.edu/drews/redacter/matchers"
	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// MacAddress is the struct for this matcher
type MacAddress struct{}

// Regex returns the regex for a match
func (m MacAddress) Regex() *regexp.Regexp {
	return regexp.MustCompile(`([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})`)
}

// Obfuscator returns the default obfuscator for this matcher
func (m MacAddress) Obfuscator() obfuscators.Creator {
	return obfuscators.Obfuscators["macaddress"]
}

var _ matchers.Matcher = MacAddress{}

func init() {
	matchers.Add("macaddress", func(m string) matchers.Matcher {
		return &MacAddress{}
	})
}
