package macaddress

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMacAddressMatcher(t *testing.T) {
	tests := map[string]struct {
		given  string
		expect bool
	}{
		"simple":   {given: "00-B0-D0-63-C2-26", expect: true},
		"colons":   {given: "00:B0:D0:63:C2:26", expect: true},
		"no-match": {given: "foo bar", expect: false},
	}
	for desc, tt := range tests {
		got, _ := regexp.Match(MacAddress{}.Regex().String(), []byte(tt.given))
		require.Equal(t, tt.expect, got, desc)
	}
}
