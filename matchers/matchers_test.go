package matchers_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/drews/redacter/matchers"
	_ "gitlab.oit.duke.edu/drews/redacter/matchers/all"
)

/*
func TestMacAddressMatcher(t *testing.T) {
	tests := map[string]struct {
		given  string
		expect bool
	}{
		"mac":       {given: "00:00:00:11:22:33", expect: true},
		"not-a-mac": {given: "8.8.8.8", expect: false},
	}

	for desc, tt := range tests {
		got, _ := regexp.Match(MacAddressMatcher.String(), []byte(tt.given))
		require.Equal(t, tt.expect, got, desc)
	}
}
*/

func TestNew(t *testing.T) {
	got, err := matchers.New(map[string]any{
		"match": "foo",
	})
	require.NoError(t, err)
	require.NotNil(t, got)

	// Test empty match
	got, err = matchers.New(map[string]any{})
	require.NoError(t, err)
	require.NotNil(t, got)

	// Test bad regex
	_, err = matchers.New(map[string]any{
		"match": "foo)",
	})
	require.Error(t, err)
	require.EqualError(t, err, "error parsing regexp: unexpected ): `foo)`")

	// Custom matcher
	_, err = matchers.New(map[string]any{
		"matcher": "ipv4",
	})
	require.NoError(t, err)

	// Missing matcher
	_, err = matchers.New(map[string]any{
		"matcher": "never-exists",
	})
	require.Error(t, err)
	require.EqualError(t, err, "could not find built in matcher: never-exists")
}
