/*
Package matchers defines how matchers behave
*/
package matchers

import (
	"fmt"
	"regexp"

	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// Matcher describes the needed functions for an item to be considered a Matcher
type Matcher interface {
	Regex() *regexp.Regexp
	Obfuscator() obfuscators.Creator
}

// Creator creates a Matcher
type Creator func(string) Matcher

// Matchers is a map of names to matchers
var Matchers = map[string]Creator{}

// Add just adds a new matcher plugin
func Add(name string, creator Creator) {
	Matchers[name] = creator
}

// Get Retrieves a creator and an error
func Get(name string) (Creator, error) {
	if c, ok := Matchers[name]; ok {
		return c, nil
	}
	return nil, fmt.Errorf("unknown matcher: %s", name)
}

// MustGet get's, but panics on err
func MustGet(name string) Creator {
	r, err := Get(name)
	if err != nil {
		panic(err)
	}
	return r
}

// Default returns the default matcher for a given string
func Default(w string) string {
	return "regex"
}

// MatcherRegex is a list of regexes for a given matcher
// type MatcherRegex *regexp.Regexp

// IPv6Matcher matches any ipv6 address
var IPv6Matcher *regexp.Regexp = regexp.MustCompile(`(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))`)

// New returns a Matcher from a given map
func New(v map[string]any) (Matcher, error) {
	var matcherS, matchS string
	var ok bool
	if matchS, ok = v["match"].(string); !ok {
		matchS = ""
	} else {
		// Ensure a valid regex
		if _, rerr := regexp.Compile(matchS); rerr != nil {
			return nil, rerr
		}
	}
	// Did we specify a custom matcher?
	if matcherS, ok = v["matcher"].(string); ok {
		if cmr, err := Get(matcherS); err == nil {
			return cmr(matchS), nil
		}
		return nil, fmt.Errorf("could not find built in matcher: %v", matcherS)
	}

	return MustGet("regex")(matchS), nil
}
