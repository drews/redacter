/*
Package redacter redacts and swaps sensitive values in a byte stream
*/
package redacter

import (
	"fmt"
	"io"

	"gitlab.oit.duke.edu/drews/redacter/matchers"
	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// Prefix to use for special string findings and such
// const redacterPrefix string = "redacter:"

// Redacter describes what a redacter does
type Redacter interface {
	Process([]byte) []byte
}

// Client is the operator for this stuff
type Client struct {
	Actions      ActionList
	replacements ReplacementHistory
	debugWriter  io.Writer
}

// ReplacementHistory is a list of things that got replaced
type ReplacementHistory map[string][]byte

// Add adds a new entry to the ReplacementHistory

// Config holds both swaps and redacts, and maybe whatever else we want for a client
type Config struct {
	Actions ActionList `yaml:"actions,omitempty"`
}

// Action represents an item that will be redacted, swapped, obfuscated, etc
type Action struct {
	Match   string           `yaml:"match"`
	matcher matchers.Matcher `yaml:"-"`
	// matchre     *regexp.Regexp         `yaml:"-"`
	With       string                 `yaml:"with"`
	Obfuscator string                 `yaml:"obfuscator,omitempty"`
	obfuscator obfuscators.Obfuscator `yaml:"-"`
}

func withWith(v map[string]any) string {
	if withS, ok := v["with"].(string); ok {
		return withS
	}
	return ""
}

func withObfuscator(v map[string]any) string {
	if obf, ok := v["obfuscator"].(string); ok {
		return obf
	}
	return ""
}

// UnmarshalYAML does the custom unmarshalling needed
func (a *Action) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var v map[string]any
	if err := unmarshal(&v); err != nil {
		return err
	}
	var merr error
	// a.matcher, merr = matcherWith(v)
	a.matcher, merr = matchers.New(v)
	if merr != nil {
		return merr
	}
	withS := withWith(v)

	// Are we specifying an obfuscator in the action?
	obfName := withObfuscator(v)
	if obfName != "" {
		obff, oberr := obfuscators.Get(obfName)
		if oberr != nil {
			return fmt.Errorf("invalid obfuscator listed: %v", obfName)
		}
		a.obfuscator = obff(withS)
	} else {
		// Does our matcher recommend an obfuscator?
		if (a.matcher != nil) && (a.matcher.Obfuscator() != nil) {
			a.obfuscator = a.matcher.Obfuscator()(withS)
		} else {
			// if given 'With', use the replacer, otherwise just censor
			obff, oerr := obfuscators.Get(obfuscators.Default(withS))
			if oerr != nil {
				return oerr
			}
			a.obfuscator = obff(withS)
		}
	}

	return nil
}

// History is a structured version of the history
type History struct {
	Replacements ReplacementHistory `yaml:"replacements"`
}

// History shows the replacements and redactions performed
func (c *Client) History() History {
	return History{
		Replacements: c.replacements,
	}
}

// ActionList represents multiple Action items
type ActionList []Action

// Process runs all the redacters
func (c *Client) Process(v []byte) []byte {
	return c.process(v)
}

func (c *Client) runAction(a Action, b []byte) ([]byte, error) {
	// Default to just the censor obfuscator
	if a.obfuscator == nil {
		a.obfuscator = obfuscators.Obfuscators["censor"]("")
	}
	if a.matcher == nil {
		cmr, err := matchers.Get("censor")
		if err != nil {
			return nil, err
		}
		a.matcher = cmr(a.With)
	}
	return a.matcher.Regex().ReplaceAllFunc(b, func(sb []byte) []byte {
		// var repl []byte
		if got, ok := c.replacements[string(sb)]; ok {
			return a.matcher.Regex().ReplaceAll(sb, got)
			// repl = got
		}
		repl := a.obfuscator.Obfuscate(sb)
		c.replacements[string(sb)] = repl
		return a.matcher.Regex().ReplaceAll(sb, repl)
	}), nil
}

func (c *Client) debug(msg string) {
	fmt.Fprint(c.debugWriter, msg)
}

// process goes through the actions
func (c *Client) process(v []byte) []byte {
	var err error
	var prev []byte
	c.debug(fmt.Sprintf("Working on: %v\n", string(v)))
	for _, item := range c.Actions {
		c.debug(fmt.Sprintf("Run Action: %+v\n", item))
		v, err = c.runAction(item, v)
		panicIfErr(err)
		if prev == nil {
			prev = v
		}

		if string(v) != string(prev) {
			c.debug(fmt.Sprintf("Change Result: %v\n", string(v)))
		}

		prev = v
	}
	return v
}

// Ensure our client complies with the Redacter interface
var _ Redacter = (*Client)(nil)

// WithDebugWriter sets the io.Writer for debug messages
func WithDebugWriter(w io.Writer) func(*Client) {
	return func(c *Client) {
		c.debugWriter = w
	}
}

// WithActions applies an ActionList to the new client
func WithActions(a ActionList) func(*Client) {
	return func(c *Client) {
		c.Actions = a
	}
}

// WithConfig applies a config object to the client
func WithConfig(s Config) func(*Client) {
	return func(c *Client) {
		c.Actions = s.Actions
	}
}

// New returns a new redacter object
func New(options ...func(*Client)) *Client {
	c := &Client{
		debugWriter: io.Discard,
	}

	for _, o := range options {
		o(c)
	}
	// Init replacements map
	c.replacements = ReplacementHistory{}

	return c
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
