package censor

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCensor(t *testing.T) {
	require.Equal(
		t,
		[]byte("XXX"),
		Censor{}.Obfuscate([]byte("foo")),
	)
}
