/*
Package censor provides an obfuscator to do a full censor
*/
package censor

import (
	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// Censor takes a byte string and replaces it with
type Censor struct{}

// Obfuscate does the actual obfuscation
func (c Censor) Obfuscate(b []byte) []byte {
	out := []rune(string(b))
	for idx := range out {
		out[idx] = 'X'
	}
	return []byte(string(out))
}

func init() {
	obfuscators.Add("censor", func(string) obfuscators.Obfuscator {
		return &Censor{}
	})
}
