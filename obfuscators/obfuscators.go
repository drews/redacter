/*
Package obfuscators sets up some generic pieces of what an Obfuscator is
*/
package obfuscators

import "fmt"

/*
type randomDomain struct{}

func (r randomDomain) Obfuscate([]byte) []byte {
	return []byte(strings.ToLower(faker.DomainName()))
}

type randomSubDomain struct{}

func (r randomSubDomain) Obfuscate(b []byte) []byte {
	pieces := strings.Split(string(b), ".")
	if len(pieces) == 1 {
		return []byte(strings.ToLower(faker.Word()))
	}
	pieces[0] = strings.ToLower(faker.Word())
	return []byte(strings.Join(pieces, "."))
}
*/

/*


 */

/*
type randomMacAddress struct{}

func (r randomMacAddress) Obfuscate([]byte) []byte {
	return []byte(faker.MacAddress())
}

type randomSerial struct{}

func (r randomSerial) Obfuscate([]byte) []byte {
	u := uuid.New()
	short := strings.ReplaceAll(u.String(), "-", "")
	return []byte(short)
}
*/

// Obfuscator describes an obfuscation function
type Obfuscator interface {
	Obfuscate([]byte) []byte
}

// Creator creats new obfuscator instances
type Creator func(string) Obfuscator

// Obfuscators is a map of strings to obfuscator creators
var Obfuscators = map[string]Creator{}

// Add just adds a new obfuscator plugin
func Add(name string, creator Creator) {
	Obfuscators[name] = creator
}

// Get Retrieves a creator and an error
func Get(name string) (Creator, error) {
	if c, ok := Obfuscators[name]; ok {
		return c, nil
	}
	return nil, fmt.Errorf("unknown creator: %s", name)
}

// Default returns the default obfuscator, as either 'censor' or 'replace' if given a string
func Default(w string) string {
	if w == "" {
		return "censor"
	}
	return "replace"
}
