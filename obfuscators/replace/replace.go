/*
Package replace does a replace style redaction
*/
package replace

import (
	"bytes"

	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// Replacer replaces a byte string
type Replacer struct {
	with string
}

// Obfuscate satisfies the Obfuscate interface
func (r Replacer) Obfuscate(b []byte) []byte {
	return bytes.ReplaceAll(b, b, []byte(r.with))
}

func init() {
	obfuscators.Add("replace", func(w string) obfuscators.Obfuscator {
		if w == "" {
			panic("must pass 'with' to NewReplacer")
		}
		return &Replacer{with: w}
	})
}
