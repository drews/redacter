package replace

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestReplace(t *testing.T) {
	require.Equal(t, []byte("bar"), Replacer{with: "bar"}.Obfuscate([]byte("foo")))
}
