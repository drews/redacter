package obfuscators

import (
	"testing"

	"github.com/stretchr/testify/require"
	// _ "gitlab.oit.duke.edu/drews/redacter/obfuscators/censor" // register all the plugins
)

func TestAdd(t *testing.T) {
	Add("foo", func(w string) Obfuscator {
		return nil
	})
	_, err := Get("foo")
	require.NoError(t, err)

	_, err = Get("never-exists")
	require.Error(t, err)
	require.EqualError(t, err, "unknown creator: never-exists")
}

func TestDefaults(t *testing.T) {
	require.Equal(t, "censor", Default(""))
	require.Equal(t, "replace", Default("some-text"))
}
