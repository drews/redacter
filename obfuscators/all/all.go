/*
Package all initializes all the plugins
*/
package all

import (
	_ "gitlab.oit.duke.edu/drews/redacter/obfuscators/censor" // register all the plugins
	_ "gitlab.oit.duke.edu/drews/redacter/obfuscators/ipv4"
	_ "gitlab.oit.duke.edu/drews/redacter/obfuscators/macaddress"
	_ "gitlab.oit.duke.edu/drews/redacter/obfuscators/replace"
	_ "gitlab.oit.duke.edu/drews/redacter/obfuscators/subdomain"
)
