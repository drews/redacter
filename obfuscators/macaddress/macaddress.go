/*
Package macaddress describes how to obfuscate a macaddress
*/
package macaddress

import (
	"github.com/go-faker/faker/v4"
	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// MacAddress is the main struct for this type
type MacAddress struct{}

// Obfuscate fulfills the Obfuscator interface
func (m *MacAddress) Obfuscate([]byte) []byte {
	return []byte(faker.MacAddress())
}

func init() {
	obfuscators.Add("macaddress", func(w string) obfuscators.Obfuscator {
		r := MacAddress{}
		var ret obfuscators.Obfuscator = &r
		return ret
	})
}
