/*
Package subdomain replaces a subdomain with a random word
*/
package subdomain

import (
	"strings"

	"github.com/go-faker/faker/v4"
	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// SubDomain is the main struct for this type
type SubDomain struct{}

// Obfuscate fulfills the Obfuscator interface
func (s *SubDomain) Obfuscate(b []byte) []byte {
	pieces := strings.Split(string(b), ".")
	if len(pieces) == 1 {
		return []byte(strings.ToLower(faker.Word()))
	}
	pieces[0] = strings.ToLower(faker.Word())
	return []byte(strings.Join(pieces, "."))
}

func init() {
	obfuscators.Add("subdomain", func(w string) obfuscators.Obfuscator {
		r := SubDomain{}
		var ret obfuscators.Obfuscator = &r
		return ret
	})
}
