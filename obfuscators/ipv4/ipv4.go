/*
Package ipv4 generates ip v4 based obfuscations
*/
package ipv4

import (
	"net/netip"

	"github.com/go-faker/faker/v4"
	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
)

// IPv4 is the main struct for this type
type IPv4 struct {
	prefix *netip.Prefix
	cur    netip.Addr
}

// Obfuscate fulfills the Obfuscator interface
func (r *IPv4) Obfuscate([]byte) []byte {
	// If no prefix specified, just use a totally random one
	if r.prefix == nil {
		return []byte(faker.IPv4())
	}
	// Go ahead and increment, we wanna skip the first one
	r.cur = r.cur.Next()

	return []byte(r.cur.String())
}

func init() {
	obfuscators.Add("ipv4", func(w string) obfuscators.Obfuscator {
		if w == "" {
			// Empty lil randomizer
			tr := IPv4{}
			var tf obfuscators.Obfuscator = &tr
			return tf
		}
		a, err := netip.ParsePrefix(w)
		if err != nil {
			panic(err)
		}

		r := IPv4{
			prefix: &a,
			cur:    a.Addr(),
		}
		var ret obfuscators.Obfuscator = &r
		return ret
	})
}
