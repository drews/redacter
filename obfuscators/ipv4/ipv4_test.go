package ipv4

import (
	"net/netip"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRandomIP(t *testing.T) {
	i := IPv4{}
	require.NotEqual(
		t,
		[]byte("192.168.1.5"),
		i.Obfuscate([]byte("192.168.1.5")),
	)
}

func TestIPObfuscatorWith(t *testing.T) {
	// Single IP
	p, _ := netip.ParsePrefix("192.168.1.0/24")
	i := IPv4{
		prefix: &p,
		cur:    p.Addr(),
	}
	require.Equal(
		t,
		[]byte("192.168.1.1"),
		i.Obfuscate([]byte("1.2.3.4")),
	)
}
