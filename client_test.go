package redacter

import (
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/drews/redacter/matchers"
	_ "gitlab.oit.duke.edu/drews/redacter/matchers/all"
	"gitlab.oit.duke.edu/drews/redacter/obfuscators"
	_ "gitlab.oit.duke.edu/drews/redacter/obfuscators/all"
	"gopkg.in/yaml.v2"
)

func TestNewClient(t *testing.T) {
	require.NotNil(t, New())
	require.NotNil(t, New(WithActions(ActionList{})))
}

func TestWithDebugWriter(t *testing.T) {
	require.Equal(t, io.Discard, New().debugWriter)
	require.Equal(t, os.Stderr, New(WithDebugWriter(os.Stderr)).debugWriter)
}

func TestAction(t *testing.T) {
	got, err := New().runAction(
		Action{
			matcher:    matchers.Matchers["regex"]("my-company"),
			obfuscator: obfuscators.Obfuscators["replace"]("example"),
		}, []byte("my-company.com"))
	require.NoError(t, err)
	require.Equal(t, []byte("example.com"), got)

	got, err = New().runAction(
		Action{
			matcher:    matchers.Matchers["regex"]("my-company"),
			obfuscator: obfuscators.Obfuscators["censor"](""),
		}, []byte("my-company.com"))
	require.NoError(t, err)
	require.Equal(t, []byte("XXXXXXXXXX.com"), got)

	// Test multi line
	got, err = New().runAction(
		Action{
			matcher:    matchers.Matchers["regex"]("my-company"),
			obfuscator: obfuscators.Obfuscators["censor"](""),
		}, []byte("my-company.com\nWebsite at www.my-company.com"))
	require.NoError(t, err)
	require.Equal(t, []byte("XXXXXXXXXX.com\nWebsite at www.XXXXXXXXXX.com"), got)
}

func TestActionMatch(t *testing.T) {
	c := New(WithActions(
		ActionList{
			Action{
				matcher:    matchers.Matchers["regex"]("foo"),
				obfuscator: obfuscators.Obfuscators["replace"]("bar"),
			},
		},
	))
	require.NotNil(t, c)

	require.Equal(
		t,
		[]byte("bar.com"),
		c.Process([]byte("foo.com")),
	)
}

func TestActionNoMatch(t *testing.T) {
	c := New(WithActions(
		ActionList{
			Action{
				matcher: matchers.Matchers["regex"]("baz"),
				With:    "bar",
			},
		},
	))
	require.NotNil(t, c)

	require.Equal(t, []byte("foo.com"), c.Process([]byte("foo.com")))
}

func TestActionUnmarshal(t *testing.T) {
	var v Action
	b := []byte("---\nmatch: foo\nwith: bar")
	err := yaml.Unmarshal(b, &v)
	require.NoError(t, err)
}

func TestActionUnmarshalInvalid(t *testing.T) {
	var v Action
	b := []byte("---\nmatch: foo)\nwith: bar)")
	err := yaml.Unmarshal(b, &v)
	require.Error(t, err)
	require.EqualError(t, err, "error parsing regexp: unexpected ): `foo)`")
}

/*
	func TestRedactMatch(t *testing.T) {
		c := New(WithActions(
			ActionList{
				Action{
					// Match: regx{regEx: regexp.MustCompile("foo")},
					matchre: regexp.MustCompile("foo"),
				},
			},
		))
		require.NotNil(t, c)

		require.Equal(
			t,
			[]byte("XXX.com"),
			c.Process([]byte("foo.com")),
		)
	}

	func TestUnmarshalAction(t *testing.T) {
		tests := map[string]struct {
			b         []byte
			expect    Action
			expectErr string
		}{
			"simple": {
				b: []byte(`match: "unmarshal-foo"`),
				expect: Action{
					matchre:     regexp.MustCompile("unmarshal-foo"),
					obfuscatorf: censor{},
				},
			},
		}
		for desc, tt := range tests {
			var got Action
			err := yaml.Unmarshal(tt.b, &got)
			if tt.expectErr != "" {
				require.Error(t, err, desc)
				require.EqualError(t, err, tt.expectErr, desc)
			} else {
				require.NoError(t, err, desc)
				require.EqualValues(t, tt.expect, got, desc)
			}
		}
	}

	func TestBuiltinMatcher(t *testing.T) {
		var a Action
		err := yaml.Unmarshal([]byte("match: redacter:ipv4"), &a)
		require.NoError(t, err)
		got, err := New().runAction(a, []byte("Hello 192.168.1.1"))
		require.NoError(t, err)
		require.NotNil(t, got)
		require.NotEqual(t, []byte("Hello 192.168.1.1"), got)
	}
*/
func TestReplaceWith(t *testing.T) {
	var a Action
	err := yaml.Unmarshal([]byte("match: thing.foo.com\nwith: example.com"), &a)
	require.NoError(t, err)
	// fmt.Fprintf(os.Stderr, "ACTION: %+v\n", a)
	// fmt.Fprintf(os.Stderr, "MATCHER: %+v\n", a.matcher)
	got, err := New().runAction(a, []byte("bar.thing.foo.com"))
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, []byte("bar.example.com"), got)
}

func TestActionObfuscatorUnmarshal(t *testing.T) {
	var v Action
	b := []byte("---\nmatch: foo\nobfuscator: censor")
	err := yaml.Unmarshal(b, &v)
	require.NoError(t, err)
}
