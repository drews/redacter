# Redacter

Redact information that you don't want in your stubs, examples, outputs, etc.
Configured using a `~/.redacter.yaml` file with how to handle redactions.