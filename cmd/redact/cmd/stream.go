package cmd

import (
	"bufio"
	"fmt"
	"os"
	"path"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/drews/redacter"
	"gopkg.in/yaml.v3"
)

// streamCmd represents the stream command
var streamCmd = &cobra.Command{
	Use:   "stream",
	Short: "Stream input and redact the output",
	Run: func(cmd *cobra.Command, args []string) {
		changed, err := cmd.Flags().GetBool("changed")
		checkErr(err, "Could not get history")
		h, err := homedir.Dir()
		checkErr(err, "")
		configf := path.Join(h, ".redacter.yaml")
		b, err := os.ReadFile(configf) // nolint:gosec
		checkErr(err, "error reading config")
		var config redacter.Config
		err = yaml.Unmarshal(b, &config)
		checkErr(err, "")

		// opts := []redactor
		opts := []func(*redacter.Client){
			redacter.WithConfig(config),
		}
		if verbose {
			opts = append(opts, redacter.WithDebugWriter(os.Stderr))
		}
		r := redacter.New(opts...)

		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			fmt.Println(string(r.Process(scanner.Bytes())))
		}
		checkErr(scanner.Err(), "error scanning stdin")

		if changed {
			for k, v := range r.History().Replacements {
				log.Info().Str("replaced", k).Str("with", string(v)).Send()
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(streamCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// streamCmd.PersistentFlags().String("foo", "", "A help for foo")
	streamCmd.PersistentFlags().Bool("changed", false, "Show a log of the things that were changed")
}
